import React, { Component } from 'react';
import Game from './container/game';
import Start from './container/start';
import './App.css';

class App extends Component {
  render() {
    return (<Game />);
  }
}

export default App;
