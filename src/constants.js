import keyMirror from 'keymirror';

export const ACTIONS = keyMirror({
  SET_VISIBILITY_FILTER: null,
  ADD_TODO: null
});
