import React, { Component } from 'react';

class Button extends Component {
  render() {
    const { text, onClick, disabled } = this.props;
    return (
      <button className="button" disabled={disabled} onClick={onClick}>
        <span className="button-text">{ text }</span>
      </button>
    );
  }
}

export default Button;