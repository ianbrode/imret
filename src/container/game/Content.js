import React, { Component } from 'react';

class Content extends Component {

  state = {
    cards: [
      '_one',
      '_two',
      '_three',
      '_four',
      '_five',
      '_six',
      '_seven',
      '_eight',
      '_nine',
      '_ten',
      '_eleven',
      '_twelve',
      '_thirteen',
      '_fourteen',
      '_fifteen',
      '_sixteen',
      '_seventeen',
      '_eighteen',
      '_nineteen',
      '_twenty',
      '_twenty-one',
      '_twenty-two',
      '_twenty-three',
      '_twenty-four',
      '_twenty-five',
      '_twenty-six',
      '_twenty-seven',
      '_twenty-eight',
      '_twenty-nine',
      '_thirty'
    ]
  }

  render() {
    return (
      <div className="game_content">
        <ul className="playing-cards">
          {
            this.state.cards.map((c, i) => {
              let wasted = '';
              let icon = '';
              let found = this.props.openedCards.indexOf(`${i + 1}`);
              if (found !== -1) {
                wasted = '_wasted';
                icon = `icon gift${this.props.prizes[found]}`;
              } 
              let className = `playing-card ${c} ${wasted} ${icon}`;

              return (
                <li key={i} className={className} onClick={() => {
                  if (wasted) return;
                  if (this.props.tries == 0 && !this.props.isGuest) {
                    this.props.callback(true, 'call_for_help')
                  } else {
                    this.props.checkCard(i+1);  
                  }
                }}></li>
              );
            })
          }
        </ul>
      </div>
    );
  }
}

export default Content;