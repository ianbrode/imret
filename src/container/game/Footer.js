import React, { Component } from 'react';

class Footer extends Component {
  countPrizes = (arr) => {
    let a = [];
    let b = []
    let prev;
    
    arr.sort();
    for ( let i = 0; i < arr.length; i++ ) {
      if (arr[i] !== prev) {
        a.push(arr[i]);
        b.push(1);
      } else {
        b[b.length-1]++;
      }
      prev = arr[i];
    }
    
    return [a, b];
  }

  countThemWell = () => {
    const pri = this.countPrizes(this.props.prizes);
    let result = [];
    pri[0].forEach((e, i) => {
      const majorPrize = parseInt(e, 10) < 0;
      const isPrize = pri[1][i] >= (majorPrize ? 5 : 3);
      if (isPrize) result.push(e);
    })
    return result;
  }

  render() {
    const prizes = this.countThemWell();
    const style = ( (prizes.length == 0 && !this.props.isGuest) || this.props.isGuest) ? {opacity: '0.5'} : {};
    return (
      <div className="game_footer">
        <div className="footer-get-gifts_green">
          <div className="footer-get-gifts_white">
            <span style={style} className="footer-get-gifts_text" onClick={() => {
              if (prizes.length == 0 || this.props.isGuest) return;
              this.props.callback(true, 'email')
            }}>Получить призы</span>
          </div>
        </div>
        { this.props.rules &&
          <div id="rules" className="footer-rules">
            <h2 className="footer-header">Правила игры</h2>
            <div className="footer-content">
              <h3 className="footer-content_title">Правила и условия проведения конкурса «Играй и в Имеретинском отдыхай!»</h3>
              <p className="footer-content_txt">(далее - Правила)</p>
              <h3 className="footer-content_title">1. Общие положения</h3>
              <p className="footer-content_txt">
                1.1. Организатором конкурса «Играй и в 'Имеретинском' отдыхай!» (далее – Конкурс) является ООО «Медиа контент».<br /> 
                ООО «Медиа контент» осуществляет техническое и организационное обеспечение Конкурса. 
              </p>
              <p className="footer-content_txt">
                ООО «Медиа контент»:<br />
                127015, Москва, Бумажный проезд, дом 14, стр. 2<br />
                ИНН: 7743824265<br />
                ОГРН: 1117746576056
              </p>
              <p className="footer-content_txt">
                1.2. Конкурс проводится в соответствии с настоящими Правилами и действующим законодательством Российской Федерации. Конкурс не является лотереей, стимулирующей лотереей либо иной основанной на риске азартной игрой.
              </p>
              <p className="footer-content_txt">
                1.3. Конкурс проводится с 15.05.2017 по 30.06.2017 г для жителей России.
              </p> 
              <p className="footer-content_txt">
                1.4. К участию (далее – «Участник») в Конкурсе допускаются только физические лица, граждане РФ, достигшие 18-ти лет. Сотрудники ООО «Медиа контент» и члены их семей к участию в розыгрыше не допускаются.
              </p>
              <p className="footer-content_txt">
                1.5. Принимая участие в Конкурсе, Участник подтверждает, что ознакомлен и соглашается с настоящими Правилами и дает ООО «Медиа контент» и уполномоченным Организатором Конкурса лицам согласие на обработку и распространение своих персональных данных, использование своего изображения, биографических подробностей в целях исполнения настоящего Конкурса.<br />
                Персональные данные Участника, предоставляемые им Организатору Конкурса, будут использоваться Организатором исключительно в связи с настоящим Конкурсом и не будут предоставляться никаким третьим лицам для целей, не связанных с Конкурсом, за исключением случаев, прямо предусмотренных законодательством РФ. 
              </p>
              <h3 className="footer-content_title">2. Порядок проведения конкурса и определение победителей. Призы победителям</h3>
              <p className="footer-content_txt">2.1. Для участия в Конкурсе Участнику необходимо в период с 15.05.2017 по 30.06.2017 зарегистрироваться в конкурсе (указать свою электронную почту, имя или авторизоваться через социальные сети) и открыть три карточки в игровом поле.
              </p>
              <p className="footer-content_txt">
                2.2. Для получения приза, игроку необходимо пригласить в игру до девяти друзей и открыть как можно больше игровых карточек. Первые 832 участника игры, открывшие три одинаковые призовые карточки получают призы от ООО «РогСибАл» и ООО «Клиника ЛМС».
              </p>
              <p className="footer-content_txt">
                2.3. Список призов:
              </p>
                <ul>
                  <li>Неделя проживания на двоих по тарифу «Всё включено»</li>
                  <li>Weekend по системе «Все включено» на двоих</li>
                  <li>Неделя проживания для двоих в апарт-отеле (завтрак шведский стол)</li>
                  <li> Перелёт для двоих по маршруту Москва-Сочи-Москва или Санкт-Петербург-Сочи-Санкт-Петербург + 2 ночи проживания на двоих в «Имеретинский» 4*</li>
                  <li>Посещение фитнес-центра Отеля 4*</li>
                  <li>Day pass для двоих на пляжный клуб «Имеретинский»</li>
                  <li>Сертификат от БЗ</li>
                  <li>Сертификат в СПА на двоих</li>
                  <li>Ужин на двоих в одном из ресторанов ГК «Имеретинский»</li>
                  <li>1 уровень карты лояльности</li>
                  <li>2 уровень карты лояльности</li>
                  <li> 3 уровень карты лояльности</li>
                  <li>Встреча в аэроопрту Трансфер "Аэропорт-отель"</li>
                </ul>
              <p className="footer-content_txt">
                <a href="https://docs.google.com/spreadsheets/d/1F9iEqG82dOuSzMndeXVkQoejJu6mRipdIZe87HRJ6dA/edit#gid=0" target="blank">Подробное описание призов</a><br />
                  Проезд к месту вручения приза организатор не обеспечивает и не оплачивает.
              </p>
              <p className="footer-content_txt">
                2.4. Результаты будут объявлены не позднее 31.08. 2017 года.
              </p>
              <p className="footer-content_txt">
                2.5. Вручение призов состоится представителем ООО «РогСибАл», ООО «Клиника ЛМС». 
              </p>
              <p className="footer-content_txt">
                2.6. Победитель вправе отказаться от получения приза. Право на получение приза не может быть передано победителем другому лицу.
              </p>
              <p className="footer-content_txt">
                2.7. Участник может стать Победителем только один раз за весь период проведения Конкурса, при этом в случае победы члены семьи Победителя не обладают правом участия в Конкурсе в дальнейшем. 
              </p>
              <p className="footer-content_txt">
                2.8. Участник не может обменять сертификат на аналогичную сумму в денежном эквиваленте.
              </p>
              <h3 className="footer-content_title">3. Права и обязанности участника и организатора конкурса</h3>
              <p className="footer-content_txt">3.1. Организатор Конкурса вправе не вступать в письменные переговоры либо иные контакты с Участниками, кроме случаев, прямо предусмотренных настоящими Правилами и действующим законодательством Российской Федерации.</p>
              <p className="footer-content_txt">3.2. Организатор вправе затребовать в случае необходимости, а Победитель обязан предоставить информацию, необходимую Организатору</p>
              <p className="footer-content_txt">3.3. Конкурс является публичным и открытым, в связи с чем Организатор вправе предоставлять информацию об участнике Конкурса третьим лицам.</p>
              <p className="footer-content_txt">3.4. Организатор Конкурса не несет ответственности:</p>
                <ul>
                  <li>за неполучение Участником уведомления о победе в Конкурсе;</li>
                  <li>за сбои в работе операторов связи, непосредственно обслуживающих участников Конкурса;</li>
                  <li>за системные сбои и другие технические неполадки;</li>
                  <li>в случае возникновения форс-мажорных обстоятельств;</li>
                  <li>за невыполнение (несвоевременное выполнение) Участниками обязательств, связанных с участием в Конкурсе; </li>
                  <li>за не ознакомление Участников с Правилами проведения и условиями участия в Конкурсе, а равно их неверную трактовку.</li>
                </ul>
              <p className="footer-content_txt">3.5. Победитель должен самостоятельно исчислить и уплатить сумму НДФЛ с дохода в виде стоимости полученного приза, превышающей 4000,00 рублей, в порядке, предусмотренном ст. 228-229 НК РФ.</p>
              <p className="footer-content_txt">Организатор информирует победителей о порядке получения призов и необходимости исполнения требований налогового законодательства Российской Федерации. Участник обязуется предоставить Организатору всю необходимую информацию и документы, необходимые в соответствии с налоговым законодательством для исполнения обязанностей налогового агента.</p>
              <h3 className="footer-content_title">4. Заключительные положения</h3>
              <p className="footer-content_txt">4.1. При подготовке, проведении и участии в Конкурсе не допускается нарушение законодательства РФ и настоящих Правил.</p>
              <p className="footer-content_txt">4.2. Настоящие Правила могут быть изменены Организатором с предварительным уведомлением Участников на портале за 3 (три) календарных дня до внесения изменений.</p>
              <div className="footer-about">
                <p className="footer-about_contacts">
                  Организатор Конкурса:<br /> 
                  ООО «Медиа контент»<br />
                  Юридический адрес: 127015, Москва, Бумажный проезд, дом 14, стр. 2<br />
                  Фактический адрес: 127015, Москва, Бумажный проезд, дом 14, стр. 2<br />
                  Телефон: (495) 663-38-11<br />
                  Факс: (495) 663-38-18<br />
                  ИНН:7743824265<br />
                  КПП:771401001<br />
                  ОГРН:1117746576056
                </p>
                <p className="footer-about_contacts">
                  Реквизиты банка:<br />
                  БИК 044525225<br />
                  ПАО СБЕРБАНК г. Москва<br />
                  Р/с 40702810438000104274<br />
                  К/с 30101810400000000225
                </p>
                <p className="footer-about_more">Подробнее о конкурсе - 8 800 707 77 22, <a href="mailto:more@imeretinka.ru">more@imeretinka.ru</a></p>
              </div>
            </div>
          </div>
        }
      </div>
    );
  }
}

export default Footer;