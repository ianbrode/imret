import React, { Component } from 'react';
import Try from './Try.js';

class Header extends Component {
  render() {
    const {
      callback,
      isGuest,
      showSide
    } = this.props;
    return (
      <div className="header">
        <div className="header-logo">
          <span className="header-logo_text">Побеждай и в 'Имеретинском' отдыхай!</span>
          <span className="header-logo_life">
            <a
              href="https://life.ru"
              target="_blank"
              className="header-logo_life-img"
            ></a>
          </span>
          <a href="http://imeretinka.ru" target="_blank" className="header-logo_imeret">
            <div className="header-logo_imeret-triangle">
              <i className="header-logo_imeret-img"></i>
            </div>
          </a>
          <span className="header-logo_gift" onClick={showSide}></span>
        </div>
        <span className="header-try">
          <span className="header-try_text">Осталось попыток:</span>
          <Try tries={this.props.tries}/>
          { !isGuest && <span className="header-try_cross-button" onClick={() => {
              callback(true, 'soc')
            }}></span>
          }
          <a href="#rules" className="header-rules" onClick={() => {
            this.props.setRules();
          }}>Правила игры</a>
        </span>
      </div>
    );
  }
}

export default Header;