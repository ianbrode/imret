import React, { Component } from 'react';
import PopupWelcome         from './PopupWelcome.js';
import PopupWin             from './PopupWin.js';
import PopupAuth            from './PopupAuth.js';
import PopupEmail           from './PopupEmail.js';
import PopupSoc             from './PopupSoc.js';
import PopupError           from './PopupError.js';
import PopupContact         from './PopupContact.js';
import PopupSuccess         from './PopupSuccess.js';
import PopupHelperWelcome   from './PopupHelperWelcome.js';
import PopupHelperPlay      from './PopupHelperPlay.js';
import PopupCallForHelp     from './PopupCallForHelp.js';
import PopupWinList         from './PopupWinList'
import PopupWinListFriend   from './PopupWinListFriend'
import PopupAuthHelper      from './PopupAuthHelper';
import PopupNonHelperPlay   from './PopupNonHelperPlay';

class Popup extends Component {
  render() {
    const {
      type,
      active,
      tries,
      roll,
      callback,
      isRegistered,
      isGuest,
      link,
      prizes,
      reset
    } = this.props;

    let Pop;
    let activeClass;

    switch(type) {
      case 'error':
        Pop = <PopupError />
        break;
      case 'contact':
        Pop = <PopupContact onClick={callback} />
        break;
      case 'success':
        Pop = <PopupSuccess onClick={callback} />
        break;
      case 'non_helper_play':
        Pop = <PopupNonHelperPlay
          onClick={callback}
          reset={reset}
        />
        break;
      case 'helper_play':
        Pop = <PopupHelperPlay
          onClick={callback}
          reset={reset}
        />
        break;
      case 'auth_helper':
        Pop = <PopupAuthHelper
          onClick={callback}
        />
        break;
      case 'win_list_friend':
        Pop = <PopupWinListFriend
          reset={reset}
          prizes={prizes}
          onClick={callback}
        />
        break;
      case 'win_list':
        Pop = <PopupWinList
          prizes={prizes}
          onClick={callback}
        />
        break;
      case 'call_for_help':
        Pop = <PopupCallForHelp
          onClick={callback}
          link={link}
        />
        break;
      case 'helper':
        Pop = <PopupHelperWelcome
          onClick={callback}
        />
        break;
      case 'soc':
        Pop = <PopupSoc
          onClick={callback}
          link={link}
        />
        break;
      case 'email':
        Pop = <PopupEmail
          onClick={callback}
          prizes={prizes}
        />
        break;
      case 'auth':
        Pop = <PopupAuth
          onClick={callback}
        />
        break;
      case 'win':
        Pop = <PopupWin
          onClick={callback}
          tries={tries}
          roll={roll}
          isRegistered={isRegistered}
          isGuest={isGuest}
        />
        break;
      default:
        Pop = <PopupWelcome
          onClick={callback}
          tries={tries}
        />
    }

    activeClass = active ? '' : '_no-active';

    return (
      <div className={`popup ${activeClass}`}>
        <div className="popup-overlay">
          <div className="popup-inner">
            { Pop }
          </div>
        </div>
      </div>
    );
  }
}

export default Popup;
