import React, { Component } from 'react';
import Button from './Button.js';
import Vk from '../../img/social/group.jpg'
import Fb from '../../img/social/fb.jpg'
import Ok from '../../img/social/group-2.jpg'

class PopupWelcome extends Component {
  render() {
    return (
      <div className="popup-welcome">
        <div className="popup-header">Авторизация</div>
        <div className="popup-content">Для получения призов, пожалуйста, авторизуйтесь.</div>
        <div style={{textAlign: 'center'}}>
          <a style={{padding: '10px'}} href='https://projects.life.ru/imer/ok.php?connect=1'>
            <img src={Ok}/>
          </a>
          <a style={{padding: '10px'}} href='https://projects.life.ru/imer/vk.php?connect=1'>
            <img src={Vk}/>
          </a>
          <a style={{padding: '10px'}} href='https://projects.life.ru/imer/fb.php?connect=1'>
            <img src={Fb}/>
          </a>
        </div>
      </div>
    );
  }
}

export default PopupWelcome;