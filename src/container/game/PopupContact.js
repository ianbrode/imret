import React, { Component } from 'react';
import Button from './Button.js';

class PopupWelcome extends Component {
  render() {
    return (
      <div className="popup-welcome">
        <div className="popup-header">Поздравляем!</div>
        <div className="popup-contact">По вопросам получения призов обращаться к Чернокальской Нине Александровне. Телефон +7 862 241 90 69 доб.812-088. Nina.chernokalskaya@imeretinka.ru</div>
        <Button onClick={() => {
          this.props.onClick(true, 'email');
        }} text='Отправить сертификат еще раз!'/>
        <div className="popup-footer">
          <div className="popup-rules"></div>
        </div>
      </div>
    );
  }
}

export default PopupWelcome;