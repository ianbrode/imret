import React, { Component } from 'react';
import Button from './Button.js';
import Checkbox from './Checkbox.js';

class PopupEmail extends Component {
  state = {
    email: '',
    checkbox: false,
    canSubmit: false
  }

   countPrizes = (arr) => {
    let a = [];
    let b = []
    let prev;
    
    arr.sort();
    for ( let i = 0; i < arr.length; i++ ) {
      if (arr[i] !== prev) {
        a.push(arr[i]);
        b.push(1);
      } else {
        b[b.length-1]++;
      }
      prev = arr[i];
    }
    
    return [a, b];
  }

  countThemWell = () => {
    const pri = this.countPrizes(this.props.prizes);
    let result = [];
    pri[0].forEach((e, i) => {
      const majorPrize = parseInt(e, 10) < 0;
      const isPrize = pri[1][i] >= (majorPrize ? 5 : 3);
      if (isPrize) result.push(e);
    })
    return result;
  } 

  sendEmail() {
    const { onClick } = this.props;
    fetch(`https://projects.life.ru/imer/app.php?type=email&email=${encodeURIComponent(this.state.email)}`, {
      method: "GET",
      credentials: 'same-origin',
      mode: 'cors'
    }).then(() => {
      // if (this.countThemWell().length == 2) {
      //   onClick(true, 'contact');
      // } else {
      onClick(true, 'success'); 
      // }
    });
  }

  render() {
    return (
      <div className="popup-welcome">
        <div className="popup-close" onClick={() => {
          this.props.onClick(false, null)
        }}>закрыть</div>
        <div className="popup-header">Получить призы</div>
        <div className="popup-content">Поздравляем! Чтобы получить сертификат на призы от Имеретинского, напишите нам свой e-mail:</div>

        <input
          style={{
            fontSize: '24px',
            padding: '20px',
            width: '80%',
            margin: '20px auto'
          }}
          onChange={(e) => this.setState({email: e.target.value})}
        />
        <Checkbox
          style={{
            fontSize: '16px',
            margin: '20px auto'
          }}
          handleCheckboxChange={(l, checked) => {
            if (!checked && this.state.email) this.setState({canSubmit: true});
          }} 
          label='Я согласен на обработку персональных данных, получение информации о призах, в соответствии с условиями, указанными по ссылке'
        />

        <Button disabled={!this.state.canSubmit} onClick={() => {
          this.sendEmail();
        }} text='Получить сертификат'/>
      </div>
    );
  }
}

export default PopupEmail;