import React, { Component } from 'react';
import Button from './Button.js';

class PopupWelcome extends Component {
  render() {
    return (
      <div className="popup-welcome">
        <div className="popup-header">Ой!</div>
        <div className="popup-content">Что-то пошло нет так. Перезагрузите страницу.</div>
        <div className="popup-footer">
          <div className="popup-rules"></div>
        </div>
      </div>
    );
  }
}

export default PopupWelcome;