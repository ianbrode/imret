import React, { Component } from 'react';
import Button from './Button.js';

class PopupWelcome extends Component {
  render() {
    return (
      <div className="popup-welcome">
        <div className="popup-header">Помоги другу!</div>
        <div className="popup-content">Открой 3 карточки для друга, помоги ему выиграть главный приз от “Имеретинского”!</div>
        <Button onClick={() => {
          this.props.onClick(false, null);
        }} text='Помочь другу!'/>
        <div className="popup-footer">
        </div>
      </div>
    );
  }
}

export default PopupWelcome;