import React, { Component } from 'react';
import Button from './Button.js';

class PopupWelcome extends Component {
  render() {
    return (
      <div className="popup-welcome">
        <div className="popup-header">Спасибо!</div>
        <div className="popup-content">Ваш друг уже забрал призы. Попробуйте сами выиграть призы от Имеретинского!</div>

        <a style={{textDecoration: 'none'}} className="button" href='https://projects.life.ru/imer/app.php?type=init&home=1'>
          <span className="button-text" style={{textAlign: 'center'}}>Получить свои призы</span>
        </a>
        <div className="popup-footer">
          <div className="popup-rules"></div>
        </div>
      </div>
    );
  }
}

export default PopupWelcome;
