import React, { Component } from 'react';
import Button from './Button.js';
import Vk from '../../img/social/group.jpg'
import Fb from '../../img/social/fb.jpg'
import Ok from '../../img/social/group-2.jpg'

class PopupSoc extends Component {

  makeLinks() {
    const img = 'https://projects.life.ru/imer/share.png';
    const desk = `Участвуй в конкурсе от Имеретинского и выигрывай главные призы! ${this.props.link}`;
    const title = 'Помоги мне выиграть призы';

    this.fbLink = 'https://www.facebook.com/dialog/feed?app_id=547377158772591' +
      '&redirect_uri=https://projects.life.ru/imer/' +
      '&link=' + encodeURIComponent(this.props.link) +
      '&name=' + encodeURIComponent(title) +
      '&href=' + encodeURIComponent(this.props.link) +
      '&picture=' + img +
      '&caption=' + encodeURIComponent(desk) +
      '&description=' + encodeURIComponent(desk);

    this.vkLink = 'https://vk.com/share.php?' +
      'url=' + encodeURIComponent(this.props.link) +
      '&title=' + encodeURIComponent(title) +
      '&description=' + encodeURIComponent(desk) +
      '&image=' + img;

    this.mateLink = 'https://connect.ok.ru/dk?st.cmd=WidgetSharePreview' +
      '&st.shareUrl=' + encodeURIComponent(this.props.link) +
      '&st.title=' + encodeURIComponent(title) +
      '&st.description='+ encodeURIComponent(desk);

    // this.mateLink = 'https://connect.ok.ru/offer?url=' + encodeURIComponent(this.props.link) +
    //   '&imageUrl=' + img + 
    //   '&title=' + encodeURIComponent(title) +
    //   '&description='+ encodeURIComponent(desk);
  }

  render() {
    this.makeLinks();
    return (
      <div className="popup-welcome">
        <div className="popup-header">Помощь друзей</div>
        <div className="popup-content">Попроси друзей помочь тебе выиграть главный приз от Имеретинского.</div>
        <div style={{
          textAlign: 'center',
          padding: '20px',
          marginBottom: '20px'
        }}>
          <a target="_blank" style={{padding: '10px'}} href={this.mateLink}>
            <img src={Ok}/>
          </a>
          <a target="_blank" style={{padding: '10px'}} href={this.vkLink}>
            <img src={Vk}/>
          </a>
          <a target="_blank" style={{padding: '10px'}} href={this.fbLink}>
            <img src={Fb}/>
          </a>
        </div>
        <Button onClick={() => {
          this.props.onClick(false, null);
        }} text='Закрыть'/>
      </div>
    );
  }
}

export default PopupSoc;
