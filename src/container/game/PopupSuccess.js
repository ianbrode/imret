import React, { Component } from 'react';
import Button from './Button.js';

class PopupWelcome extends Component {
  render() {
    return (
      <div className="popup-welcome">
        <div className="popup-header">Поздравляем!</div>
        <div className="popup-content">Сертификат выслан на вашу почту!</div>
        <Button onClick={() => {
          this.props.onClick(false, null);
        }} text='Ура!'/>
        <div className="popup-footer">
          <div className="popup-rules"></div>
        </div>
      </div>
    );
  }
}

export default PopupWelcome;