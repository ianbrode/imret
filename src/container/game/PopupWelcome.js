import React, { Component } from 'react';
import Button from './Button.js';

class PopupWelcome extends Component {
  render() {
    let numerical = 'попытки';
    if (this.props.tries == 0) numerical = 'попыток';
    if (this.props.tries == 1) numerical = 'попыткa';

    return (
      <div className="popup-welcome">
        <div className="popup-header">Добро пожаловать!</div>
        <div className="popup-content">Открой как можно больше карточек на игровом поле и получи главный приз от “Имеретинского”!</div>
        <Button onClick={() => {
          this.props.onClick(false, null);
        }} text='Играть!'/>
        <div className="popup-footer">
          <div className="popup-info">У вас {this.props.tries} {numerical}</div>
          <div className="popup-rules"></div>
        </div>
      </div>
    );
  }
}

export default PopupWelcome;