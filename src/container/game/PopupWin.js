import React, { Component } from 'react';
import Button from './Button.js';
import { pri } from './SidebarContent';

class PopupWelcome extends Component {
  render() {
    let numerical = 'попытки';
    if (this.props.tries == 0) numerical = 'попыток';
    if (this.props.tries == 1) numerical = 'попыткa';

    return (
      <div className="popup-welcome">
        <div className="popup-header">Поздравляем!</div>

        { pri[this.props.roll](0) }

        <Button onClick={() => {
          if (this.props.tries == 1 && this.props.isRegistered === false && this.props.isGuest === false) {
            this.props.onClick(true, 'auth');
          } else {
            this.props.onClick(false, null);  
          }
        }} text='Ура!'/>
        <div className="popup-footer">
          <div className="popup-info">У вас {this.props.tries} {numerical}</div>
          <div className="popup-rules"></div>
        </div>
      </div>
    );
  }
}

export default PopupWelcome;