import React, { Component } from 'react';
import Button from './Button.js';
import { pri2 } from './SidebarContent';

class PopupWinList extends Component {
  countPrizes = (arr) => {
    let a = [];
    let b = []
    let prev;
    
    arr.sort();
    for ( let i = 0; i < arr.length; i++ ) {
      if (arr[i] !== prev) {
        a.push(arr[i]);
        b.push(1);
      } else {
        b[b.length-1]++;
      }
      prev = arr[i];
    }
    
    return [a, b];
  }

  countThemWell = () => {
    const pri = this.countPrizes(this.props.prizes);
    let result = [];
    pri[0].forEach((e, i) => {
      const majorPrize = parseInt(e, 10) < 0;
      const isPrize = pri[1][i] >= (majorPrize ? 5 : 3);
      if (isPrize) result.push(e);
    })
    return result;
  }

  render() {
    const prizes = this.countThemWell();

    return (
      <div className="popup-welcome">
        <div className="popup-close" onClick={() => {
          this.props.onClick(false, null)
        }}>закрыть</div>
        <div className="popup-header">Поздравляем!</div>

        {
          prizes.map((p, i) => pri2[p](i))
        }

        <Button onClick={() => {
          this.props.onClick(true, 'email');  
        }} text='Получить призы'/>
        <div className="popup-footer">
          <div className="popup-rules"></div>
        </div>
      </div>
    );
  }
}

export default PopupWinList;