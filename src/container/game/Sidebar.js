import React, { Component } from 'react';
import SidebarContent from './SidebarContent.js';

class Sidebar extends Component {
  state ={
    isWide: false,
    isSocialOpen: false
  }
  
  countPrizes = (arr) => {
    let a = [];
    let b = []
    let prev;
    
    arr.sort();
    for ( let i = 0; i < arr.length; i++ ) {
      if (arr[i] !== prev) {
        a.push(arr[i]);
        b.push(1);
      } else {
        b[b.length-1]++;
      }
      prev = arr[i];
    }
    
    return [a, b];
  }

  countThemWell = () => {
    const pri = this.countPrizes(this.props.prizes);
    let result = [];
    pri[0].forEach((e, i) => {
      const majorPrize = parseInt(e, 10) < 0;
      const isPrize = pri[1][i] >= (majorPrize ? 5 : 3);
      if (isPrize) result.push(e);
    })
    return result;
  }

  toggleWide = () => {
    this.setState({isWide: !this.state.isWide })
  }

  makeLinks() {
    const img = 'https://projects.life.ru/imer/share2.png';
    const desk = `Участвуй в конкурсе от Имеретинского и выигрывай главные призы!`;
    const title = 'Помоги мне выиграть призы';

    this.fbLink = 'https://www.facebook.com/dialog/feed?app_id=547377158772591' +
      '&redirect_uri=https://projects.life.ru/imer/' +
      '&link=https://projects.life.ru/imer/' +
      '&name=' + encodeURIComponent(title) +
      '&href=https://projects.life.ru/imer/' +
      '&picture=' + img +
      '&caption=' + encodeURIComponent(desk) +
      '&description=' + encodeURIComponent(desk);

    this.vkLink = 'https://vk.com/share.php?' +
      'url=https://projects.life.ru/imer/' +
      '&title=' + encodeURIComponent(title) +
      '&description=' + encodeURIComponent(desk) +
      '&image=' + img;

    this.mateLink = 'https://connect.ok.ru/dk?st.cmd=WidgetSharePreview' +
      '&st.shareUrl=' + encodeURIComponent(this.props.link) +
      '&st.title=' + encodeURIComponent(title) +
      '&st.description='+ encodeURIComponent(desk);

    // this.mateLink = 'https://connect.ok.ru/offer?url=' + encodeURIComponent(this.props.link) +
    //   '&imageUrl=' + img + 
    //   '&title=' + encodeURIComponent(title) +
    //   '&description='+ encodeURIComponent(desk);
  }

  render() {
    const prizes = this.countThemWell();
    const style = ( (prizes.length == 0 && !this.props.isGuest) || this.props.isGuest) ? {opacity: '0.5'} : {};
    const showSocial = this.state.isSocialOpen === true ? 'sidebar-down _show-social' : 'sidebar-down';
    this.makeLinks()
    return (
      <div className={`game_sidebar ${this.state.isWide ? '_wide' : '' }`}>
        <SidebarContent
        prizes={this.props.prizes}
        setRules={this.props.setRules}
        toggleWide={this.toggleWide}
        />
        <div className={showSocial}>
          <span style={style} onClick={() => {
            if (prizes.length == 0 || this.props.isGuest) return;
            
            this.props.callback(true, 'win_list');
          }} className="sidebar-down_show-gifts">
          </span>
          <span className="sidebar-down_share">
            <span className="sidebar-down_arrow" onClick={() => {
              let toggleSocial = !this.state.isSocialOpen;
              this.setState({isSocialOpen: toggleSocial});
            }}>
              <i className="sibebar-down_arrow-icon"></i>
            </span>
            <span className="sidebar-down_social">
              <ul className="sidebar-down_social-items">
                <li className="sidebar-down_social-item">
                  <a href={this.vkLink} className="social-item _vk"></a>
                </li>
                <li className="sidebar-down_social-item">
                  <a href={this.fbLink} className="social-item _fb"></a>
                </li>
                <li className="sidebar-down_social-item">
                  <a href={this.mateLink} className="social-item _ok"></a>
                </li>
              </ul>
            </span>
          </span>
        </div>
      </div>
    );
  }
}

export default Sidebar;