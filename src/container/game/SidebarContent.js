import React, { Component } from 'react';

export const pri = {
  '-4': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-4"></i>
      </span>
      <span className="sidebar-content_gift-text">
      Перелёт для двоих по маршруту Москва-Сочи-Москва или Санкт-Петербург-Сочи-Санкт-Петербург + 2 ночи проживания на двоих в  «Имеретинский» 4<sup>*</sup> (Собери 5 картинок)</span>
    </li>
  ),
  '-3': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-3"></i>
      </span>
      <span className="sidebar-content_gift-text">
      Неделя проживания для двоих в апарт-отеле (завтрак - шведский стол) (Собери 5 картинок)</span>
    </li>
  ),
  '-1': (i) => (
    <li key={i} className="sidebar-content_gift-item">
    <span className="sidebar-content_gift-icon">
      <i className="sidebar-content_gift gift-1"></i>
    </span>
    <span className="sidebar-content_gift-text">Неделя проживания для двоих по тарифу «Всё включено» (Собери 5 картинок)
    </span>
    </li>
  ),
  '-2': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-2"></i>
      </span>
      <span className="sidebar-content_gift-text">
      Weekend по системе «Все включено» на двоих (Собери 5 картинок)</span>
    </li>
  ),
  '1': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-5"></i>
      </span>
      <span className="sidebar-content_gift-text">Посещение фитнес-центра Отеля 4<sup>*</sup> (Собери 3 картинки)
      </span>
    </li>
  ),
  '2': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-6"></i>
      </span>
      <span className="sidebar-content_gift-text">Day pass для двоих на пляжный клуб «Имеретинский» (Собери 3 картинки)
      </span>
    </li>  
  ),
  '3': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-7"></i>
      </span>
      <span className="sidebar-content_gift-text">Скидка 50% для двух человек, на услуги по прейскуранту Клиники ЛМС в г.&nbsp;Сочи (Собери 3 картинки)
      </span>
    </li>
  ),
  '4': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-8"></i>
      </span>
      <span className="sidebar-content_gift-text">Сертификат в СПА на двоих (Собери 3 картинки)
      </span>
    </li>
  ),
  '5': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-9"></i>
      </span>
      <span className="sidebar-content_gift-text">Ужин на двоих в ресторане «Босфор» или в панорамном ресторане «Lounge & Grill» (Собери 3 картинки)
      </span>
    </li> 
  ),
  '6': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-10"></i>
      </span>
      <span className="sidebar-content_gift-text">Карта лояльности ур. 1 (Собери 3 картинки)
      </span>
    </li>    
  ),
  '7': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-11"></i>
      </span>
      <span className="sidebar-content_gift-text">Карта лояльности ур. 2 (Собери 3 картинки)
      </span>
    </li>
  ),
  '8': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-12"></i>
      </span>
      <span className="sidebar-content_gift-text">Карта лояльности ур. 3 (Собери 3 картинки)
      </span>
    </li>
  ),
  '9': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-13"></i>
      </span>
      <span className="sidebar-content_gift-text">
      Встреча в аэропорту «Трансфер «Аэропорт-отель» (Собери 3 картинки)</span>
    </li>
  ),
}

export const pri2 = {
  '-4': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-4"></i>
      </span>
      <span className="sidebar-content_gift-text-alt">
      Перелёт для двоих по маршруту Москва-Сочи-Москва или Санкт-Петербург-Сочи-Санкт-Петербург + 2 ночи проживания на двоих в  «Имеретинский» 4<sup>*</sup> (Собери 5 картинок)</span>
    </li>
  ),
  '-3': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-3"></i>
      </span>
      <span className="sidebar-content_gift-text-alt">
      Неделя проживания для двоих в апарт-отеле (завтрак - шведский стол) (Собери 5 картинок)</span>
    </li>
  ),
  '-1': (i) => (
    <li key={i} className="sidebar-content_gift-item">
    <span className="sidebar-content_gift-icon">
      <i className="sidebar-content_gift gift-1"></i>
    </span>
    <span className="sidebar-content_gift-text-alt">Неделя проживания для двоих по тарифу «Всё включено» (Собери 5 картинок)
    </span>
    </li>
  ),
  '-2': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-2"></i>
      </span>
      <span className="sidebar-content_gift-text-alt">
      Weekend по системе «Все включено» на двоих (Собери 5 картинок)</span>
    </li>
  ),
  '1': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-5"></i>
      </span>
      <span className="sidebar-content_gift-text-alt">Посещение фитнес-центра Отеля 4<sup>*</sup> (Собери 3 картинки)
      </span>
    </li>
  ),
  '2': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-6"></i>
      </span>
      <span className="sidebar-content_gift-text-alt">Day pass для двоих на пляжный клуб «Имеретинский» (Собери 3 картинки)
      </span>
    </li>  
  ),
  '3': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-7"></i>
      </span>
      <span className="sidebar-content_gift-text-alt">Скидка 50% для двух человек, на услуги по прейскуранту Клиники ЛМС в г.&nbsp;Сочи (Собери 3 картинки)
      </span>
    </li>
  ),
  '4': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-8"></i>
      </span>
      <span className="sidebar-content_gift-text-alt">Сертификат в СПА на двоих (Собери 3 картинки)
      </span>
    </li>
  ),
  '5': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-9"></i>
      </span>
      <span className="sidebar-content_gift-text-alt">Ужин на двоих в ресторане «Босфор» или в панорамном ресторане «Lounge & Grill» (Собери 3 картинки)
      </span>
    </li> 
  ),
  '6': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-10"></i>
      </span>
      <span className="sidebar-content_gift-text-alt">Карта лояльности ур. 1 (Собери 3 картинки)
      </span>
    </li>    
  ),
  '7': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-11"></i>
      </span>
      <span className="sidebar-content_gift-text-alt">Карта лояльности ур. 2 (Собери 3 картинки)
      </span>
    </li>
  ),
  '8': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-12"></i>
      </span>
      <span className="sidebar-content_gift-text-alt">Карта лояльности ур. 3 (Собери 3 картинки)
      </span>
    </li>
  ),
  '9': (i) => (
    <li key={i} className="sidebar-content_gift-item">
      <span className="sidebar-content_gift-icon">
        <i className="sidebar-content_gift gift-13"></i>
      </span>
      <span className="sidebar-content_gift-text-alt">
      Встреча в аэропорту «Трансфер «Аэропорт-отель» (Собери 3 картинки)</span>
    </li>
  ),
}


class SidebarContent extends Component {
  render() {
    let keys = Object.keys(pri).reverse();

    return (
      <div className="sidebar-content">
        <div className="sidebar-content_header" onClick={this.props.toggleWide}> 
          Призы
        </div>
        <ul className="sidebar-content_gifts-list">
          { keys.map((p, i) => {
            return pri[p](i);
          }) }
          <li className="sidebar-content_rules" onClick={this.props.setRules}><a href="#rules" onClick={() => {
            this.props.setRules();
          }}>Правила игры</a></li>
        </ul>
      </div>
    );
  }
}

export default SidebarContent;