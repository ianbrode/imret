import React, { Component } from 'react';
// import TryItem from './TryItem.js';
// _waste

class Try extends Component {
  render() {
    const { tries } = this.props;
    return (
      <span className="try-items">
        <span className={`try-item ${ tries < 3 ? '_waste' : '' }`}></span>
        <span className={`try-item ${ tries < 2 ? '_waste' : '' }`}></span>
        <span className={`try-item ${ tries < 1 ? '_waste' : '' }`}></span>
      </span>
    );
  }
}

export default Try;