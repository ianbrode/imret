import React, { Component } from 'react';

class TryItem extends Component {
  render() {
    return (
      <span className="try-item">
        <span className="try-item_inner-icon"></span>
      </span>
    );
  }
}

export default TryItem;