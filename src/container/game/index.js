import React, { Component } from 'react';
import Header               from './Header';
import Content              from './Content';
import Sidebar              from './Sidebar';
import Footer               from './Footer';
import Popup                from './Popup';
import Start                from './../start';
import '../assets/css/index.css';

class Game extends Component {
  state = {
    cards: [],
    prizes: [],
    tries: 3,
    popupOpened: true,
    popupType: null,
    isRegistered: false,
    isGuest: false,
    started: false,
    rules: false,
    sidemenuOpened: false
  }

  checkCard = (id) => {
    if (this.state.tries == 1 && !this.state.isRegistered && !this.state.isGuest) {
      this.controlPopup(true, 'auth');
      return;
    };

    if (!this.state.tries && this.state.isGuest) {
      this.controlPopup(true, 'helper_play');
      return;      
    }

    const twoPrizes = this.countThemWell().length == 2;

    if (twoPrizes && this.state.hasMail && !this.state.isGuest) {
      this.controlPopup(true, 'contact');
    }

    if (twoPrizes && !this.state.hasMail && !this.state.isGuest) {
      this.controlPopup(true, 'email');
    }

    fetch(`https://projects.life.ru/imer/app.php?type=play&card=${id}`, {
      method: "GET",
      credentials: 'same-origin',
      mode: 'cors'
    }).then((res) => res.json()).then((r) => {

      if (r.errors || r.success === false) this.controlPopup(true, 'error');

      if (!r.tries && r.isGuest) {
        setTimeout(() => {
          this.controlPopup(true, 'helper_play');
        }, 2000);
      }

      this.setState(r);

      if (r.roll != 0) {
        let i = 0;
        r.prizes.forEach((p) => {
          if (p == r.roll) i++;
        })
        if ( ((i / 3) == Math.floor(i / 3)) && parseInt(r.roll, 10) > 0 ) this.controlPopup(true, 'win');
        if ( ((i / 5) == Math.floor(i / 5)) && parseInt(r.roll, 10) < 0 ) this.controlPopup(true, 'win');
      }
    })
  }

  countPrizes = (arr) => {
    let a = [];
    let b = []
    let prev;
    
    arr.sort();
    for ( let i = 0; i < arr.length; i++ ) {
      if (arr[i] !== prev) {
        a.push(arr[i]);
        b.push(1);
      } else {
        b[b.length-1]++;
      }
      prev = arr[i];
    }
    
    return [a, b];
  }

  countThemWell = () => {
    const pri = this.countPrizes(this.state.prizes);
    let result = [];
    pri[0].forEach((e, i) => {
      const majorPrize = parseInt(e, 10) < 0;
      const isPrize = pri[1][i] >= (majorPrize ? 5 : 3);
      if (isPrize) result.push(e);
    })
    return result;
  }

  getShares = () => {
    fetch('https://projects.life.ru/imer/app.php?type=share', {
      method: "GET",
      credentials: 'same-origin',
    }).then((res) => res.json()).then((r) => {
      this.setState(r);
    })
  }

  closePopup = () => {
    this.setState({popupOpened: false})
  }

  setRules = () => {
    let isRules = !this.state.rules;
    this.setState({rules: isRules});
  }

  controlPopup = (res, type) => {
    this.setState({
      popupOpened: res,
      popupType: type
    })
  }

  controlSidemenu = () => {
    let isSideMenuOpened = !this.state.sidemenuOpened;
    this.setState({
      sidemenuOpened: isSideMenuOpened
    })
  }

  reset() {
    fetch('https://projects.life.ru/imer/app.php?type=init&home=1', {
      method: "GET",
      credentials: 'same-origin'
    }).then((res) => res.json()).then((r) => {
      this.setState(r);
      this.getShares();
    })
  }

  componentDidMount() {
    fetch('https://projects.life.ru/imer/app.php?type=init', {
      method: "GET",
      credentials: 'same-origin'
    }).then((res) => res.json()).then((r) => {
      if (r.isRegistered && !r.isGuest) r.popupOpened = false;
      if (!r.isRegistered && r.isGuest) {
        r.popupOpened = true;
        r.popupType = 'auth_helper';
      }
      if (r.isRegistered && r.isGuest) {
        r.popupOpened = true;
        r.popupType = 'helper';
      }

      this.setState(r);
      this.getShares();

      const twoPrizes = this.countThemWell().length == 2;

      if ( r.isGuest && (r.hasMail || twoPrizes ) ) {
        this.controlPopup(true, 'non_helper_play');
      } else if (twoPrizes && r.hasMail && !r.isGuest) {
        this.controlPopup(true, 'contact');
      }      
    })
  }

  render() {
    const { started, isGuest, isRegistered, tries, sidemenuOpened } = this.state;
    return (
      <div style={{height: '100%', position: 'relative'}}>
        {
          (started || isGuest || isRegistered)
          ? <div className={`page game ${sidemenuOpened === true ? '_sidemenu' : ''} ${tries === 0 ? 'no-tries' : '' }`}>
              <Header
                tries={this.state.tries}
                callback={this.controlPopup}
                setRules={this.setRules}
                isGuest={this.state.isGuest}
                showSide={this.controlSidemenu}
                callback={this.controlPopup}
              />
              <Content
                checkCard={this.checkCard}
                openedCards={this.state.cards}
                prizes={this.state.prizes}
                tries={this.state.tries}
                callback={this.controlPopup}
                isGuest={this.state.isGuest}
              />
              <Sidebar
                prizes={this.state.prizes}
                callback={this.controlPopup}
                isGuest={this.state.isGuest}
                setRules={this.setRules}
                
              />
              <Footer
                callback={this.controlPopup}
                rules={this.state.rules}
                prizes={this.state.prizes}
                isGuest={this.state.isGuest}
              />
              <Popup
                type={this.state.popupType}
                active={this.state.popupOpened}
                callback={this.controlPopup}
                tries={this.state.tries}
                roll={this.state.roll}
                isRegistered={this.state.isRegistered}
                isGuest={this.state.isGuest}
                link={this.state.link}
                prizes={this.state.prizes}
                reset={this.reset}
              />
            </div>
          : <Start handleClick={() => {
            this.setState({started: true})
          }} />
        }
      </div>
    );
  }
}

export default Game;
