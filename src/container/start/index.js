import React, { Component } from 'react';
import '../assets/css/index.css';

class Start extends Component {
  render() {
    return (
      <div className="page start">
        <div className="start-flag">
          <div className="start-logo"></div>
          <div className="start-button">
            <div className="start-line"></div>
            <div className="start-play" onClick={this.props.handleClick}>Играть</div>
            <div className="start-line"></div>
          </div>
        </div>
      </div>
    );
  }
}

export default Start;
