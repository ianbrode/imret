import React            from 'react';
import { render }       from 'react-dom';
import { Provider }     from 'react-redux';
import thunkMiddleware  from 'redux-thunk';
import { createLogger } from 'redux-logger';
import {
  createStore,
  applyMiddleware
}                       from 'redux';
import reducers         from './reducers';
import App              from './App';
import './index.css';

const loggerMiddleware = createLogger();
const preloadedState = {};

let store = createStore(
  reducers,
  preloadedState,
  applyMiddleware(
    thunkMiddleware,
    loggerMiddleware
  )
);

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
