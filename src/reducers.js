import { combineReducers }  from 'redux';
import { ACTIONS }          from './constants';

// TODO: Example reducers

const visibilityFilter = (state = 'SHOW_ALL', action) => {
  switch (action.type) {
    case ACTIONS.SET_VISIBILITY_FILTER:
      return action.filter
    default:
      return state
  }
}

const todos = (state = [], action) => {
  switch (action.type) {
    case ACTIONS.ADD_TODO:
      return [
        ...state
      ]
    default:
      return state
  }
}


const todoApp = combineReducers({
  todos,
  visibilityFilter
})

export default todoApp
